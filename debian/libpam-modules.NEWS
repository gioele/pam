pam (1.7.0-1) unstable; urgency=medium

    Starting with pam 1.7.0, pam_limits does not automatically reset the
    limits of logged in users. This means that systemd, rather than pam will
    set the defaults for things like number of open files and other resource
    limits. If limits are configured in /etc/security/limits.conf or
    /etc/security/limits.d/*.conf, these values will be respected. To
    restore the previous behavior, add the set_all option to pam_limits.
    For example in /etc/pam.d/ssh, replace:

    session    required     pam_limits.so

    with:

    session    required     pam_limits.so set_all

 -- Sam Hartman <hartmans@debian.org>  Tue, 14 Jan 2025 15:47:56 -0700
    
pam (1.5.3-7) unstable; urgency=medium

    Starting with PAM version 1.5.3, Debian supports usergroups for default
    umask of users logging in.  If the primary group name of a user
    matches their primary user name (user pat's default group is also
    called pat), then files will be group writable by default. To disable
    this use a group name that differs from the user name or add
    nousergroups to the pam_umask line in
    /etc/pam.d/common-session and
    /etc/pam.d/common-session-noninteractive:

    session optional			pam_umask.so nousergroups


 -- Sam Hartman <hartmans@debian.org>  Mon, 08 Apr 2024 16:15:58 -0600
